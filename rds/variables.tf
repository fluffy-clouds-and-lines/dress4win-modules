variable "aws_region" {
  description = "The AWS region to deploy to (e.g. us-east-1)"
  type        = string
}

variable "environment" {
  description = "environment name"
  type        = string
}

variable "engine_version" {
  type        = string
}

variable "instance_size" {
  type = string
}

variable "database_instance_name" {
  type = string
}

variable "username" {
  type = string
}

variable "password" {
  type = string
}

variable "rds_delete_protection" {
  type = bool
}