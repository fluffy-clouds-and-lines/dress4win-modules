provider "aws" {
  region = var.aws_region
}

terraform {
  backend "s3" {}

  # The latest version of Terragrunt (v0.19.0 and above) requires Terraform 0.12.0 or above.
  required_version = ">= 0.12.0"
}

module "db" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 2.0"
  skip_final_snapshot = true

  identifier = var.database_instance_name

  engine            = "mysql"
  engine_version    = var.engine_version
  instance_class    = var.instance_size
  allocated_storage = 5

  db_subnet_group_name = "${var.environment}-vpc"

  name     = var.database_instance_name
  username = var.username
  password = var.password
  port     = "3306"

  # Database Deletion Protection
  deletion_protection = var.rds_delete_protection

  apply_immediately = true

  iam_database_authentication_enabled = true

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  tags = {
    Terraform = true
    Environment = var.environment
  }

  # DB parameter group
  family = "mysql5.7"

  # DB option group
  major_engine_version = "5.7"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "${var.database_instance_name}-final"

  parameters = [
    {
      name = "character_set_client"
      value = "utf8"
    },
    {
      name = "character_set_server"
      value = "utf8"
    }
  ]

  options = [
    {
      option_name = "MARIADB_AUDIT_PLUGIN"

      option_settings = [
        {
          name  = "SERVER_AUDIT_EVENTS"
          value = "CONNECT"
        },
        {
          name  = "SERVER_AUDIT_FILE_ROTATIONS"
          value = "37"
        },
      ]
    },
  ]
}